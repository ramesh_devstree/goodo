<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    
Route::get('/', function () {
    return view('welcome');
});

// routes for logout
Route::get('logout', array(
    'as' => 'logout', 'uses' => 'Api\Admin\LoginController@doLogout'
));

// route to show the login form
Route::get('dashboard', array(
    'as' => 'dashboard', 'uses' => 'Api\Admin\DashboardController@index'
));

// routes for login
$router->group(['prefix' => 'login'], function ($router) {
    $router->get('/', array(
        'as' => 'login', 'uses' => 'Api\Admin\LoginController@showLogin'
    ));
    $router->post('/', array(
        'uses' => 'Api\Admin\LoginController@doLogin'
    ));
});

// route reset password form
Route::get('reset-password', array(
    'as' => 'reset-password-message-view', 'uses' => 'Api\Admin\UserController@resetPasswordMessageView'
))->middleware('auth');


// route reset password form
$router->group([], function ($router) {
    $router->get('password/reset/{token}', array(
        'as' => 'reset-password-view', 'uses' => 'Api\Admin\UserController@resetPasswordView'
    ));
    $router->post('password/reset/{token}', array(
        'as' => 'reset-password', 'uses' => 'Api\Admin\UserController@resetPassword'
    ));
});

// route forgot password email
$router->group(['prefix' => 'forgot-password'], function ($router) {
    $router->get('/mail', array(
        'as' => 'forgot-password-mail',
        'uses' => 'Api\Admin\ForgotPasswordController@forgotPasswordMailView'
    ));

    // route reset password form
    Route::post('/verify-mail', array(
        'as' => 'forgot-password-verify-mail',
        'uses' => 'Api\Admin\ForgotPasswordController@forgotPasswordVerifyMail'
    ));
});

// route forgot password form
$router->group([], function ($router) {
    $router->get('password/forgot/{token}', array(
        'as' => 'forgot-password-view',
        'uses' => 'Api\Admin\ForgotPasswordController@forgotPasswordView'
    ));
    $router->post('password/forgot/{token}', array(
        'as' => 'forgot-password',
        'uses' => 'Api\Admin\ForgotPasswordController@forgotPassword'
    ));
});

// route forgot password form
$router->group([], function ($router) {
    $router->post('restaurant', array(
        'as' => 'restaurant',
        'uses' => 'Api\Admin\RestaurantController@storeRestaurant'
    ));
});

// Set locale by routes
Route::get('/{lang}', function ($lang) {
    session(['locale' => $lang]);
    // return redirect('login');
});