<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/** --------- Customer API's ---------- */
Route::post('customer/login', 'Api\Customer\UserController@login')->name('login');
Route::post('customer/register', 'Api\Customer\UserController@register');
Route::post('customer/forgotpassword', 'Api\Customer\UserController@forgotpassword');
Route::post('customer/resetpassword', 'Api\Customer\UserController@resetpassword');

// Customer api with auth middleware
Route::group([
    'prefix' => "/customer", 'middleware' => ['auth:api'],
], function ($router) {

    Route::get('/profile', 'Api\Customer\UserController@userProfile');
    Route::post('/profile/update', 'Api\Customer\UserController@updateProfile');
    Route::post('/change/password', 'Api\Customer\UserController@change_password');
    
});

// Customer api without auth middleware
Route::group([
    'prefix' => "/customer", 'middleware' => ['localization:api']
], function ($router) {

    // Fetch regions
    Route::get('/config', 'Api\Customer\CommonConfigController@getRegions');

    // Get near by restaurant listing
    Route::post('/near-by-restaurant', 'Api\Customer\RestaurantController@getNearByRestaurants');

    // Get near by restaurant listing
    Route::post('/near-by-restaurant-login-user', 'Api\Customer\RestaurantController@getNearByRestaurantsForLoginUser')->middleware('auth:api');

    // Get selected restaurant details
    Route::get('/restaurant-detail/{id}', 'Api\Customer\RestaurantController@getRestaurantDetails');
    
    // Add and get review
    Route::post('/add-review', 'Api\Customer\ReviewController@storeReview')->middleware('auth:api');
    Route::get('/get-review/{restaurantId}', 'Api\Customer\ReviewController@getReviews');

    // Add to favourite restaurants
    Route::post('/add-to-favourite', 'Api\Customer\RestaurantController@storeAddToFavouriteRestaurant')->middleware('auth:api');
});


