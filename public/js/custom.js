// Action for forgot password mail verification
$('#forgotEmailForm').on('submit',function(event){
    event.preventDefault();
    email = $('#email').val();
    
    if (email === ''){
        return false;
    }

    var url = $('#email').data('url');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: url,
        method: 'POST',
        data:{
            'email': email,
        },
        success:function(response){
            if(response.status === false) {
                $(".email-success").hide();
                $(".email-error").show();
                $(".email-error").text(response.message);
            } else if(response.status === true) {
                $(".email-error").hide();
                $(".email-success").show();                
                $(".email-success").text(response.message);
            }
        }
    });
});