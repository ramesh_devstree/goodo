<?php

namespace App\Http\Controllers\Api\Customer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Helpers\ResponseHelper;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Regions;

class CommonConfigController extends Controller
{
    /**
     * @var App\Helpers\ResponseHelper
     */
    private $responseHelper;

    /**
     * Create a new controller instance.
     *
     * @param App\Helpers\ResponseHelper $responseHelper
     */
    public function __construct(
        ResponseHelper $responseHelper
    ) {
        $this->responseHelper = $responseHelper;
    }

    /**
     * Get regions listing
     * 
     * @return Illuminate\Http\JsonResponse
     */
    public function getRegions() : JsonResponse
    {
        $regions = Regions::select('id', 'name', 'image')->get();
        $result = [];

        foreach ($regions as $key => $value) {
            $result[$key]['id'] = $value->id;
            $result[$key]['name'] = $value->name;
            if (!isset($value->image)) {
                $result[$key]['image'] = env('APP_URL').'/public/customer/regions/'.config('constants.region_default_image')[0];
            } else {
                $result[$key]['image'] = env('APP_URL').'/public/customer/regions/'.$value->image;
            }
        }

        $apiCode = Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = (empty($regions)) ? trans('messages.success.MESSAGE_NO_RECORD_FOUND')
             : trans('messages.success.MESSAGE_REGIONS_LISTING');
        $apiData['region_list'] = $result;

        return $this->responseHelper->success($apiCode, $apiStatus, $apiMessage, $apiData);
    }
}
