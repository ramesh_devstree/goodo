<?php

namespace App\Http\Controllers\Api\Customer;

use App\User;
use Auth;
use DB;
use Config;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;
use App\Helpers\ResponseHelper;

class UserController extends Controller
{
    /**
     * @var App\Helpers\ResponseHelper
     */
    private $responseHelper;

    /**
     * Create a new controller instance.
     *
     * @param App\Helpers\ResponseHelper $responseHelper
     */
    public function __construct(
        ResponseHelper $responseHelper
    ) {
        $this->responseHelper = $responseHelper;
    }

    /**
     *  User Registration
     */
    public function register(Request $request)
    {
        $id = '0';
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,' . $id . ',id,deleted_at,NULL',
//            'phone_number' => 'required|unique:users,phone_number,deleted_at',
            'first_name' => 'required',
            'last_name' => 'required',
//            'password' => 'required',
            'device_type' => 'required',
            'device_token' => 'required',
            'device_id' => 'required',
            'login_type' => ['required', Rule::in(config('constants.login_type'))]
        ]);
        if ($validator->fails()) {
            $message = $validator->messages()->first();

            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
        $social_auth_id = $request->social_auth_id;
        if(empty($social_auth_id)){
           $validator = Validator::make($request->all(), [
            'phone_number' => 'required|unique:users,phone_number,deleted_at',
            'password' => 'required',
            
            ]);
            if ($validator->fails()) {
                $message = $validator->messages()->first();
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = $message;
                $apiData = (object)[];

                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            } 
        }

        $user = new User;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->password = HASH::make($request->password);
        $user->role_id = 5;
        $user->whatsapp_wechat_id = isset($request->whatsapp_wechat_id) ? $request->whatsapp_wechat_id : null;
        $user->status = 'Pending for Approval';
        $user->latitude = isset($request->latitude) ? $request->latitude : null;
        $user->longitude = isset($request->longitude) ? $request->longitude : null;
        $user->device_type = $request->device_type;
        $user->device_token = $request->device_token;
        $user->device_id = $request->device_id;
        $user->login_type = $request->login_type;
        
        if(!empty($social_auth_id)){
            $user->social_auth_id = $request->social_auth_id;
        }
//        echo '<pre>'; print_r($user); die;
        if ($user->save()) {
            $user_data = User::select('*')->where('id', $user->id)->first();
            $data = $user_data->toArray();
            $data = $this->removeNullValue($data);
            $response['code'] = 200;
            $response['status'] = true;
            $response['message'] = "Register successful";
            $response['data'] = $data;
            $response['token'] = $user->createToken('MyApp')->accessToken;
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('Something went wrong, Please try again');
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }

        return response()->json($response);
    }

    /*
     * User Login
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $message = $validator->messages()->first();
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
        $email = $request->email;
        $social_auth_id = $request->social_auth_id;
        if(!empty($social_auth_id)){ // social login
            $checksocial_auth_id = User::where('social_auth_id', $social_auth_id)->first();
            if(empty($checksocial_auth_id)){
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('You have entered an invalid social string.');
                $apiData = (object)[];
                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }
            $other_email_check = User::where('email', $email)->where('id', '<>',$checksocial_auth_id->id)->first();
            if(!empty($other_email_check)){
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('Email id already used.');
                $apiData = (object)[];
                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }
            
            $credentials = ['email' => $checksocial_auth_id->email, 'password' => ''];
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                $user->email = $request->email;
                $user->save();
                $data = $user->toArray();
                $data = $this->removeNullValue($data);

                if ($data['profile_image'] && $data['profile_image'] != '') {
                    $data['profile_image'] = env('APP_URL').'/public/customer/profile-image/'.$data['profile_image'];
                }

                $response['code'] = 200;
                $response['status'] = true;
                $response['message'] = "Login successful";
                $response['data'] = $data;
                $response['token'] = $user->createToken('MyApp')->accessToken;

                return response()->json($response);
                
            } else {
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('You have entered an invalid password');
                $apiData = (object)[];

                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }

            // $email = $checksocial_auth_id->email;
        } else { // normal login
            $checkEmailExist = User::where('email', $email)->first();
            if (empty($checkEmailExist) || $checkEmailExist == null) {

                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('You have entered an invalid email or password');
                $apiData = (object)[];

                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }

            $credentials = ['email' => $email, 'password' => $request->password];

            if (Auth::attempt($credentials)) {

                $user = Auth::user();
                
                $data = $user->toArray();
                $data = $this->removeNullValue($data);

                if ($data['profile_image'] && $data['profile_image'] != '') {
                    $data['profile_image'] = env('APP_URL').'/public/customer/profile-image/'.$data['profile_image'];
                }

                $response['code'] = 200;
                $response['status'] = true;
                $response['message'] = "Login successful";
                $response['data'] = $data;
                $response['token'] = $user->createToken('MyApp')->accessToken;

                return response()->json($response);
            } else {
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('You have entered an invalid password');
                $apiData = (object)[];

                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }
        }
        
    }

    /*
     * Get User profile
     */
    public function userProfile(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            $response['code'] = 200;
            $response['status'] = true;
            $response['message'] = "user profile fetch successfully";
            $user = $user->toArray();
            $data = $this->removeNullValue($user);
            $response['data'] = $data;

            return response()->json($response);
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('No such registered user found');
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
    }

    /*
     * Update User profile
     */
    public function updateProfile(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,' . $user->id . ',id,deleted_at,NULL',
            'phone_number' => 'required|unique:users,phone_number,' . $user->id . ',id,deleted_at,NULL',
            'first_name' => 'required',
            'last_name' => 'required',
        ]);
        if ($validator->fails()) {
            $message = $validator->messages()->first();
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }

        if ($request->file('profile_image') != null) {
            unlink(public_path('customer/profile-image/'.$user->profile_image));
            $user->profile_image = $this->uploadFile($request);
        }

        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if (isset($request->latitude)) {
            $user->latitude = $request->latitude;
        }
        if (isset($request->longitude)) {
            $user->longitude = $request->longitude;
        }
        if (isset($request->whatsapp_wechat_id)) {
            $user->whatsapp_wechat_id = $request->whatsapp_wechat_id;
        }

        if ($user->save()) {
            $data = $this->removeNullValue($user);

            if ($data['profile_image'] && $data['profile_image'] != '') {
                $data['profile_image'] = env('APP_URL').'/public/customer/profile-image/'.$data['profile_image'];
            }
            $response['code'] = 200;
            $response['status'] = true;
            $response['message'] = "Updated successful";
            $response['data'] = $data;
            return response()->json($response);
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('Something went wrong, Please try again');
            $apiData = $data;
            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
    }

    /**
     * Forgot Password
     */
    public function forgotpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
        $email = $request->email;
        $user = User::select('id', 'email_otp', 'first_name', 'last_name', 'email')->where('email', $request->email)->first();
        if ($user != null) {
            $email_otp = $this->generateOTP();
            $this->sentFrogotPaaswordOTP($request->email, $email_otp, env('APP_NAME').": Forgot Password");
            $user->update(['email_otp' => $email_otp]);
            
            $return_data = ['email_otp' => $email_otp,'id' => $user->id,'first_name' => $user->first_name,'last_name' => $user->last_name,'email' => $user->email];
            $response['code'] = 200;
            $response['status'] = true;
            $response['message'] = "Otp has been sent. please reset your password";
            $response['data'] = $return_data;
            return response()->json($response);
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('No such regsitered user found');
            $apiData = $return_data;
            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
    }

    /**
     * Reset Password
     */
    public function resetpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'password' => 'required',
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
        $user = User::select('id', 'first_name', 'last_name', 'email', 'password', 'email_otp')->where('id', $request->id)->first();
        if ($user != null) {
            if ($user->email_otp == $request->otp) {
                $user->update(['password' => Hash::make($request->password),'email_otp' => null]);
                $response['code'] = 200;
                $response['status'] = true;
                $response['message'] = "Password reset successfully";
                return response()->json($response);
            } else {
                $apiCode = Response::HTTP_FORBIDDEN;
                $apiStatus = false;
                $apiMessage = trans('OTP is invalid');
                $apiData = (object)[];
                return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            }
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('No such regsitered user found');
            $apiData = (object)[];
            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
    }

    /**
     * Change Password
     */
    public function change_password(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;

        $validator = Validator::make($request->all(), [
               'old_password' => 'required',
               'password' => 'required',
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = $message;
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
            
        }
        
        $old_password = $request->input("old_password");
        $password = $request->input("password");
        
        if (Hash::check($old_password, $user->password)) {
            $user->update(['password' => Hash::make($password)]);
            $data['id'] = $id;
            return response()->json(['code' => 200, 'status' => true, 'message' => "Password changed successfully","data"=>$data], 200);
        } else {
            $apiCode = Response::HTTP_FORBIDDEN;
            $apiStatus = false;
            $apiMessage = trans('Old Password is Incorrect');
            $apiData = (object)[];

            return $this->responseHelper->error($apiCode, $apiStatus, $apiMessage, $apiData);
        }
    }

    /*
     * Replace null values to blank string
     */
    public function removeNullValue($array)
    {
        //Convert null value to empty string
        array_walk_recursive($array, function (&$item) {
            $item = strval($item);
        });
        return $array;
    }

    /*
     * Generate OTP
     */
    public function generateOTP()
    {
        $count = 6;
        $temp_newKey = substr(number_format(time() * mt_rand(), 0, '', ''), 0, $count);
        if (strlen($temp_newKey) == 5) {
            $newKey = '0' . $temp_newKey;
        } else {
            $newKey = $temp_newKey;
        }
        return $newKey;
    }

    /*
     * Send Forgot Password Email OTP
     */
    public static function sentFrogotPaaswordOTP($email, $otp, $subject)
    {
        $email_data = array();
        $user_data = User::where('email', $email)->first();
        $email_data['email']=$email;
        $email_data['otp']=$otp;
        $email_data['name']=$user_data->first_name;
        $val = Mail::send('mailformat/customer/forgotpassword', $email_data, function ($message) use ($email,$subject) {
            $message->to($email);
            $message->subject($subject);
            $message->from(config('constants.EMAIL_FROM'));
        });
        return $val;
    }

    /*
     * Upload image file
     */
    public function uploadFile($request)
    {
        $file = $request->file('profile_image');
        $ext = $file->getClientOriginalExtension();
        $newFileName = time() . '_' . rand(0, 1000).'.'.$ext;
        $destinationPath = 'public/customer/profile-image';
        $file->move($destinationPath, $newFileName);
        return $newFileName;
    }
}
