<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Helpers\ResponseHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Restaurant;
use App\Traits\RestExceptionHandlerTrait;
use App\Review;
use Validator;
use Auth, DB;
use Illuminate\Validation\Rule;
use App\Models\RestaurantImages;
use App\Models\RestaurantFamousDishes;
use \Illuminate\Pagination\LengthAwarePaginator;
use App\Models\RestaurantLike;

class RestaurantController extends Controller
{
    use RestExceptionHandlerTrait;

    /**
     * @var App\Helpers\ResponseHelper
     */
    private $responseHelper;

    /**
     * Create a new controller instance.
     *
     * @param App\Helpers\ResponseHelper $responseHelper
     */
    public function __construct(
        ResponseHelper $responseHelper
    ) {
        $this->responseHelper = $responseHelper;
    }

    /**
     * Get near by restaurants listing
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function getNearByRestaurants(Request $request)
    {
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $sortBy = 'asc';
        $orderBy = 'distance';
         
        $restaurantsList = Restaurant::select('*', 'average_rating as rating', DB::raw('(
            (((acos(sin(('.$latitude.'*pi()/180))*sin((latitude*pi()/180))+cos(('.$latitude.'*pi()/180))*cos((latitude*pi()/180)) * cos((('.$longitude.'- longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344)
            ) as distance'));

        if (isset($request->search) && $request->search != '') {
            $searchText = $request->search;
            $restaurantsList->where(function ($query) use ($searchText) {
                $query->where('restaurants.name', 'LIKE', '%'.$searchText.'%');
            });
        }

        if ((isset($request->sort_by) && !empty($request->sort_by)) &&
            (isset($request->order_by) && !empty($request->sort_by))) {
            $sortBy = $request->sort_by;
            $orderBy = $request->order_by;
        }

        $restaurantsList->having('distance', '<=', 10)
        ->orderBy($orderBy, $sortBy);
        $queryData = $restaurantsList->paginate(config('constants.COMMON_PAGINATION'));

        $apiCode = Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = ($queryData->isEmpty()) ? trans('messages.success.MESSAGE_NO_RECORD_FOUND')
             : trans('messages.success.MESSAGE_NEAR_BY_RESTAURANTS_LISTING');
        $apiData = $queryData;

        return $this->responseHelper->successWithPagination($apiCode, $apiStatus, $apiMessage, $apiData);
    }

    /**
     * Get restaurant details
     *
     * @param int $id
     */
    public function getRestaurantDetails(Request $request, int $id)
    {
        // Default review type is restaurant
        if (isset($request->review_type)) {
            $type = $request->review_type;
        } else {
            $type = config('constants.review_type')[0];
        }

        try {
            
            $restaurantDetails = Restaurant::select('*');
            $restaurantDetails->with(['restaurantImages' => function($query){                
                $query->select('*');
            }])
            ->with(['famousDishes' => function($query){
                $query->select('*');
            }])->with(['reviews' => function($query) use ($type) {
                $query->select('id','user_id', 'type', 'restaurant_id', 'delivery_agent_id', 
                    'rating', 'review as comment', 'type')->take(config('constants.REVIEW_DISPLAY_LIMIT'))
                ->with(['users' => function ($query) {
                    $query->select('id', 'profile_image');
                }])->where(['type' => $type]);
            }]);

            $restaurantData = $restaurantDetails->findOrFail($id)->toArray();
            $resultData = $this->transformRestaurantDetails($restaurantData);

            $apiCode = Response::HTTP_OK;
            $apiStatus = true;
            $apiMessage = trans('messages.success.MESSAGE_RESTAURANT_FOUND');
            $apiData = $resultData;

            return $this->responseHelper->success(
                $apiCode,
                $apiStatus,
                $apiMessage,
                $apiData
            );
        } catch (ModelNotFoundException $e) {
            return $this->modelNotFound(
                config('constants.error_codes.ERROR_NO_RESTAURANT_FOUND'),
                trans('messages.custom_error_message.ERROR_NO_RESTAURANT_FOUND')
            );
        }
    }

    /**
     * Transform restaurant and famous dishes images attributes 
     */
    protected function transformRestaurantDetails($restaurantData) {
        foreach($restaurantData['restaurant_images'] as $key => $value) {
            $path = env('APP_URL').'public/restaurant-images/'.$value['restaurant_id'].'/'.$value['path'];
            $restaurantData['restaurant_images'][$key]['path'] = $path;
        }

        foreach($restaurantData['famous_dishes'] as $key => $value) {
            $path = env('APP_URL').'public/famous-dish-images/'.$value['restaurant_id'].'/'.$value['path'];
            $restaurantData['famous_dishes'][$key]['path'] = $path;
        }

        foreach($restaurantData['reviews'] as $key => $value) {
            $path = '';
            if (isset($value['users'])) {
                $path = env('APP_URL').'public/customer/profile-image/'.$value['users']['profile_image'];
            } else {
                $path = env('APP_URL').'public/customer/profile-image/'.config('constants.PROFILE_IMAGE');
            }

            $restaurantData['reviews'][$key]['image'] = $path;
            unset($restaurantData['reviews'][$key]['users']);
        }

        return $restaurantData;
    }

    /**
     * Add to favourite restaurants
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function storeAddToFavouriteRestaurant (Request $request) {

        $userId = Auth::id();
        $validator = Validator::make(
            $request->all(),
            [
                'restaurant_id' => 'required|exists:restaurants,id',
            ]
        );

        // If request parameter have any error
        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                false,
                $validator->errors()->first(),
                $request->toArray(),
                config('constants.error_codes.ERROR_INVALID_RESTAURANT_ID')
            );
        }

        $restaurantId = $request->restaurant_id;
        $restaurantLike = new RestaurantLike();
        $favouriteRestaurant = $restaurantLike->findFavourite($userId, $restaurantId);

        if (is_null($favouriteRestaurant)) {
            $favouriteRestaurant = $restaurantLike->addToFavourite($userId, $restaurantId);
        } else {
            $favouriteRestaurant = $restaurantLike->removeFromFavourite($userId, $restaurantId);
        }

        $favouriteRestaurant = $restaurantLike->findFavourite($userId, $restaurantId);

        // Set response data
        $apiCode = ($favouriteRestaurant !== null) ? Response::HTTP_CREATED
        : Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = ($favouriteRestaurant !== null) ?
        trans('messages.success.MESSAGE_RESTAURANT_ADDED_TO_FAVOURITE') :
        trans('messages.success.MESSAGE_RESTAURANT_DELETED_FROM_FAVOURITE');
        $apiData = ($favouriteRestaurant !== null)
        ? ['favourite_restaurant_id' => $favouriteRestaurant->id] : [];

        return $this->responseHelper->success($apiCode, $apiStatus, $apiMessage, $apiData);
    }

    /**
     * Get near by restaurants listing for login user
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function getNearByRestaurantsForLoginUser(Request $request)
    {
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $sortBy = 'asc';
        $orderBy = 'distance';
        $userId = Auth::id();

        $restaurantsList = Restaurant::select('*', 'restaurants.id as restaurant_id', 'average_rating as rating', DB::raw('(
            (((acos(sin(('.$latitude.'*pi()/180))*sin((latitude*pi()/180))+cos(('.$latitude.'*pi()/180))*cos((latitude*pi()/180)) * cos((('.$longitude.'- longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344)
            ) as distance'))
            ->leftJoin('restaurant_like', function($join){
                $join->on('restaurants.id', '=', 'restaurant_like.restaurant_id');
            })->where('restaurant_like.user_id', $userId);

        if (isset($request->search) && $request->search != '') {
            $searchText = $request->search;
            $restaurantsList->where(function ($query) use ($searchText) {
                $query->where('restaurants.name', 'LIKE', '%'.$searchText.'%');
            });
        }

        if ((isset($request->sort_by) && !empty($request->sort_by)) &&
            (isset($request->order_by) && !empty($request->sort_by))) {
            $sortBy = $request->sort_by;
            $orderBy = $request->order_by;
        }

        $restaurantsList->having('distance', '<=', 10)
        ->orderBy($orderBy, $sortBy);
        $queryData = $restaurantsList->paginate(config('constants.COMMON_PAGINATION'));

        $apiCode = Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = ($queryData->isEmpty()) ? trans('messages.success.MESSAGE_NO_RECORD_FOUND')
             : trans('messages.success.MESSAGE_NEAR_BY_RESTAURANTS_LISTING');
        $apiData = $queryData;

        return $this->responseHelper->successWithPagination($apiCode, $apiStatus, $apiMessage, $apiData);
    } 
}
