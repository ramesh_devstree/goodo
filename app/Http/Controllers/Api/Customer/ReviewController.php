<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\RestExceptionHandlerTrait;
use App\Review;
use Validator;
use Auth, DB;
use Illuminate\Validation\Rule;
use \Illuminate\Pagination\LengthAwarePaginator;
use App\Helpers\ResponseHelper;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class ReviewController extends Controller
{
    use RestExceptionHandlerTrait;

    /**
     * @var App\Helpers\ResponseHelper
     */
    private $responseHelper;

    /**
     * Create a new controller instance.
     *
     * @param App\Helpers\ResponseHelper $responseHelper
     */
    public function __construct(
        ResponseHelper $responseHelper
    ) {
        $this->responseHelper = $responseHelper;
    }

    /**
     * Stroe review and rating details of restaurant
     *
     * @param int $id
     */
    public function storeReview(Request $request)
    {
        $userId = Auth::id();
        $validator = Validator::make(
            $request->all(),
            [
                'restaurant_id' => 'required|exists:restaurants,id',
            ]
        );

        // If request parameter have any error
        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                false,
                $validator->errors()->first(),
                [],
                config('constants.error_codes.ERROR_INVALID_ADD_TO_FAVOURITE_DATA')
            );
        }

        $requestData = $request->toArray();
        $data = Review::updateOrCreate(['user_id' => $userId, 'restaurant_id' => $request->type_id], $requestData);

        $apiCode = Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = trans('messages.success.MESSAGE_REVIEW_ADDED_SUCCESSFULLY');
        $apiData = $data->toArray();

        return $this->responseHelper->success(
            $apiCode,
            $apiStatus,
            $apiMessage,
            $apiData
        );
    }

    /**
     * Stroe review and rating details of restaurant
     *
     * @param Illuminate\Http\Request $request
     * @param int $restaurantId
     */
    public function getReviews(Request $request, int $restaurantId)
    {
        if (isset($request->review_type)) {
            $reviewType = $request->review_type;
        } else {
            $reviewType = config('constants.review_type')[0];
        }

        $reviews = Review::select('id','user_id', 'rating', 'review as comment', 'type')
            ->with(['users' => function ($query) {
                $query->select('id', 'profile_image');
            }])->where(['restaurant_id' => $restaurantId, 'type' => $reviewType]);

        $queryData = $reviews->paginate(config('constants.COMMON_PAGINATION'));

        $itemsTransformed = $queryData
            ->getCollection()
            ->map(function($item) {
                if (!isset($item->users)) {
                    $user = $item->users;
                    $user['profile_image'] =  config('constants.PROFILE_IMAGE');
                    $item->users = $user;
                }
                $item->image = env('APP_URL').'public/customer/profile-image/'.$item->users['profile_image'];
                unset($item->users);
                return $item;
        })->toArray();

        $itemsTransformedAndPaginated = new LengthAwarePaginator(
            $itemsTransformed,
            $queryData->total(),
            $queryData->perPage(),
            $queryData->currentPage(), [
                'query' => [
                    'page' => $queryData->currentPage()
                ]
            ]
        );

        $apiCode = Response::HTTP_OK;
        $apiStatus = true;
        $apiMessage = (!$queryData->isEmpty()) ? trans('messages.success.MESSAGE_REVIEW_LISTED_SUCCESSFULLY') :
            trans('messages.success.NO_DATA_FOUND');
        $apiData = $itemsTransformedAndPaginated;

        return $this->responseHelper->successWithPagination(
            $apiCode,
            $apiStatus,
            $apiMessage,
            $apiData
        );
    }
}
