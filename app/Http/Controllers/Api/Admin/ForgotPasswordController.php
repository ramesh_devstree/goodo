<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Repositories\User\UserRepository;
use DB, Validator;

class ForgotPasswordController extends Controller
{
    /**
     * @var App\Repositories\User\UserRepository $userRepository
     */
    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param App\Repositories\User\UserRepository $userRepository
     * @return void
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Render view for enter mail for forgot password
     */
    public function forgotPasswordMailView()
    {
        return view('layout.forgotpassword.forgot-password-mail');
    }

    /**
     * Enter email and verify for forgot password
     * 
     * @param Request $request
     */
    public function forgotPasswordVerifyMail(Request $request)
    {
        $email = $request->email;
        $checkUser = User::select('email')->where('email', $email)->first();

        if(!isset($checkUser)) {
            $response['status'] = false;
            $response['message'] = trans("No such regsitered user found");
        } else {
            $resetPassword = $this->userRepository->resetPasswordMessage($email, 'forgot');
            if ($resetPassword) {
                $response['status'] = true;
                $response['message'] = trans("Reset password link have been sent to your registered email id, please check you inbox.");
            } else {
                $response['status'] = false;
                $response['message'] = trans("A Network Error occurred. Please try again.");
            }
        }
        
        return response()->json($response);
    }

    /**
     * Forgot password form render view
     * 
     * @param Request $request
     * @param string $token
     */
    public function forgotPasswordView(Request $request, string $token)
    {
        $email = $request->email;
        $data = DB::table('password_resets')
            ->where(['token' => $token, 'email' => $email])->first();
        
        if (!isset($data)) {
            return view('layout.forgotpassword.forgot-password')->withErrors(['invalid_link' => trans('Your link is not valid to reset a password, please try again.')]);
        }

        return view('layout.forgotpassword.forgot-password', compact('email', 'token'));
    }

    /**
     * Forgot Password by requestd data
     * 
     * @param Request $request
     * @param string $token
     */
    public function forgotPassword(Request $request, string $token)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required',
            'confirm_password' => 'required'
        ]);

        $email = $request->email;

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return view('layout.forgotpassword.forgot-password')->withErrors($validator); // send back all errors to the reset password form
        }

        $confirmPassword = $request->input("confirm_password");
        $newPassword = $request->input("new_password");

        if($newPassword !== $confirmPassword) {
            return view('layout.forgotpassword.forgot-password', compact('email', 'token'))->withErrors(['not_match_password' => trans('confirm password do not match with new password')]);
        }

        $data = $this->userRepository->forgotPassword($request);
        return view('layout.forgotpassword.forgot-password', compact('email', 'token'))->with('success', trans('Password changed successfully'));
    }
}
