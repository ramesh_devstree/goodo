<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth, DB, Validator;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * @var App\Repositories\User\UserRepository $userRepository
     */
    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param App\Repositories\User\UserRepository $userRepository
     * @return void
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Reset password link send to mail and render message view
     *
     * @param Illuminate\Http\Request $request
     */
    public function resetPasswordMessageView($request = null)
    {
        try {
            $user = Auth::user();
            $email = $user->email;

            $resetPassword = $this->userRepository->resetPasswordMessage($email, 'reset');

            if ($resetPassword) {
                return view('reset-password-message', compact('email'));
            } else {
                return view('reset-password-messageresetPasswordView')->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
            }
        } catch (\Exception $e) {
            return view('reset-password-message')->withErrors(['error' => trans('A user is not authenticated. Please try again.')]);
        }
    }

    /**
     * Reset password form render view
     * 
     * @param Request $request
     * @param string $token
     */
    public function resetPasswordView(Request $request, string $token)
    {
        $email = $request->email;
        $data = DB::table('password_resets')
            ->where(['token' => $token, 'email' => $email])->first();
        
        if (!isset($data)) {
            return view('reset-password')->withErrors(['invalid_link' => trans('Your link is not valid to reset a password, please try again.')]);
        }

        return view('reset-password', compact('email', 'token'));
    }

    /**
     * Reset Password of requested data
     * 
     * @param Request $request
     * @param string $token
     */
    public function resetPassword(Request $request, string $token)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required'
        ]);

        $email = $request->email;

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return view('reset-password')->withErrors($validator); // send back all errors to the reset password form
        }

        $confirmPassword = $request->input("confirm_password");
        $newPassword = $request->input("new_password");

        if($newPassword !== $confirmPassword) {
            return view('reset-password', compact('email', 'token'))->withErrors(['not_match_password' => trans('confirm password do not match with new password')]);
        }

        $data = $this->userRepository->resetPassword($request);

        if (!$data) {
            Auth::logout();
            return view('reset-password', compact('email', 'token'))->withErrors(['incorrect_password' => trans('Old Password is Incorrect')]);
        } else {
            return view('reset-password', compact('email', 'token'))->with('success', trans('Password changed successfully'));
        }
    }
}
