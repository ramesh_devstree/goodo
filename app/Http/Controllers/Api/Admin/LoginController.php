<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Return login view page
     */
    public function showLogin()
    {
        // Form View
        return view('login');
    }

    /**
     * Post and validate user login
     *
     * @param Illuminate\Http\Request $request
     */
    public function doLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:8'
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')->withErrors($validator); // send back all errors to the login form
        } else {
            // create our user data for the authentication
            $userdata = array(
                'email' => $request->email ,
                'password' => $request->password
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // validation successful
                return Redirect::to('dashboard');
            } else {
                // validation not successful, send back to form
                return Redirect::to('login')->withErrors(['invalid_details' => trans('Username or password is invalid')]);
            }
        }
    }

    /**
     * Logout and redirect to login page
     */
    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }
}
