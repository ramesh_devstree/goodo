<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'user_id', 'type', 'restaurant_id', 'delivery_agent_id', 'rating', 'review', 'users', 'restarants',
        'comment', 'image'
    ];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['user_id', 'type', 'restaurant_id', 'delivery_agent_id', 'rating', 'review'];

    public function users() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function restarants() {
        return $this->hasMany(Restaurant::class, 'id', 'type_id');
    }
}
