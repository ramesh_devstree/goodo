<?php

namespace App\Repositories\User;

use App\Repositories\User\UserInterface;
use DB, Mail, Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;
use Illuminate\Http\Request;

class UserRepository implements UserInterface
{
    public function __construct()
    {
    }

    /**
     * Send mail on reset password
     *
     * @param string $email
     * @param string $for check for which pirpose we set new password
     * @return boolean
     */
    public function resetPasswordMessage(string $email, string $for): bool
    {
        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);

        //Get the token just created above
        $tokenData = DB::table('password_resets')->where('email', $email)->first();

        if ($this->sendResetEmail($email, $tokenData->token, $for)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $email
     * @param string $token
     * @return boolean
     */
    private function sendResetEmail($email, $token, $for): bool
    {
        //Generate, the password reset link. The token generated is embedded in the link
        $link = config('app.url') . 'password/'. $for .'/' . $token . '?email=' . $email;
        $subject = 'Goodo: Forgot Password';

        try {
            $email_data = array();
            $user_data = User::where('email', $email)->first();
            $email_data['email']=$email;
            $email_data['link']=$link;
            $val = Mail::send('mailformat/admin/forgotpassword', $email_data, function ($message) use ($email,$subject) {
                $message->to($email);
                $message->subject($subject);
                $message->from(env('mail_from_address'));
            });
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Password reset operation
     * 
     * @param Request $request
     * @return boolean
     */
    public function resetPassword(Request $request): bool
    {
        $oldPassword = $request->input("old_password");
        $newPassword = $request->input("new_password");

        $user = User::select('password')->where(['email' => $request->email])->first();

        if (Hash::check($oldPassword, $user->password)) {
            DB::table('users')
                ->where(['email' => $request->email])
                ->update(['password' => Hash::make($newPassword)]);
            DB::table('password_resets')
            ->where(['token' => $request->token, 'email' => $request->email])->delete();
            return true;
        }

        return false;   
    }

    /**
     * Password forgot operation
     * 
     * @param Request $request
     * @return boolean
     */
    public function forgotPassword(Request $request): bool
    {
        $newPassword = $request->input("new_password");
        
        // Update user password
        DB::table('users')
            ->where(['email' => $request->email])
            ->update(['password' => Hash::make($newPassword)]);

        // Delete reset token
        DB::table('password_resets')
           ->where(['token' => $request->token, 'email' => $request->email])->delete();

        return true;
        
    }
}
