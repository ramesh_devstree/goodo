<?php
namespace App\Repositories\User;

use Illuminate\Http\Request;

interface UserInterface
{
    /**
     * Send mail on reset password
     *
     * @param string $email
     * @param string $for
     * @return boolean
     */
    public function resetPasswordMessage(string $email, string $for): bool;

    /**
     * Password reset operation
     * 
     * @param Request $request
     * @return boolean
     */
    public function resetPassword(Request $request): bool;
}
