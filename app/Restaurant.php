<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\RestaurantImages;
use App\Models\RestaurantFamousDishes;
use App\Models\RestaurantLike;

class Restaurant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name', 'address', 'area', 'postcode', 'image', 'latitude', 'longitude', 'phone_number',
        'status', 'activation_date', 'expiry_date', 'max_table_number', 'max_menu_item', 'average_rating', 'distance', 'avg_rating',
        'restaurantImages', 'famousDishes', 'reviews', 'distance_unit'
    ];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name', 'address', 'area', 'postcode', 'image', 'latitude', 'longitude', 'phone_number',
    'status', 'activation_date', 'expiry_date', 'max_table_number', 'max_menu_item', 'average_rating'];

    /**
     * Get restaurant images
     */
    public function restaurantImages(): HasMany {
        return $this->hasMany(RestaurantImages::class, 'restaurant_id', 'id');
    }

    /**
     * Get Famous dishes info and images
     */
    public function famousDishes(): HasMany {
        return $this->hasMany(RestaurantFamousDishes::class, 'restaurant_id', 'id');
    }

    /**
     * Get first 10 reviews
     */
    public function reviews(): HasMany {
        return $this->hasMany(Review::class, 'restaurant_id', 'id');
    }

    /**
     * Get liked restaurants
     */
    public function likeRestaurant(int $userId): HasMany {
        return $this->hasMany(RestaurantLike::class, 'restaurant_id', 'id')->where('user_id', $userId);
    }
}
