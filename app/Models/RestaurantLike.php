<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class RestaurantLike extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant_like';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'restaurant_id', 'user_id'];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['restaurant_id', 'user_id'];

    /**
     * Store/update specified resource.
     *
     * @param  int  $userId
     * @param  int  $restaurantId
     * @return bool
     */
    public function addToFavourite(int $userId, int $restaurantId): bool
    {
        return static::withTrashed()->updateOrCreate(
            ['user_id' => $userId, 'restaurant_id' => $restaurantId]
        )->restore();
    }

    /**
     * Delete the specified resource.
     *
     * @param  int  $userId
     * @param  int  $restaurantId
     * @return bool
     */
    public function removeFromFavourite(int $userId, int $restaurantId): bool
    {
        return static::where(['user_id' => $userId, 'restaurant_id' => $restaurantId])->delete();
    }

    /**
     * Find specified resource.
     *
     * @param  int  $userId
     * @param  int  $missionId
     * @return null|App\Models\RestaurantLike
     */
    public function findFavourite(int $userId, int $restaurantId): ?RestaurantLike
    {
        return static::where('restaurant_id', $restaurantId)->where('user_id', $userId)->first();
    }
}
