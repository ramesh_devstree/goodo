<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantFamousDishes extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'famous_dishes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id',  'restaurant_id', 'name', 'path'];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['restaurant_id', 'name', 'path'];
}
