<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes,HasApiTokens;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_image', 'first_name', 'last_name', 'email', 'password', 'phone_number', 'whatsapp_wechat_id', 'status', 'approved_by', 'approved_date', 'comment', 'latitude', 'longitude', 'email_otp',
        'device_type', 'device_token', 'device_id', 'login_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function Role(){
        return $this->hasOne('App\Role','id','role_id');
    }

    public function AauthAcessToken() {
        return $this->hasMany('\App\OauthAccessToken');
    }

}
