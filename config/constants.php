<?php

//file : app/config/constants.php

return [
    'SITE_NAME' => 'Goodo',
    'AppName' => 'Goodo',
    'EMAIL_FROM' => env('MAIL_FROM_ADDRESS'),
    'COMMON_PAGINATION' => 10,
    'PROFILE_IMAGE' => 'profile.png',
    'REVIEW_DISPLAY_LIMIT' => 10,

    'login_type' => [
        'google',
        'facebook',
        'email'
    ],

    'region_default_image' => [
        'Macau.png'
    ],

    'error_codes' => [
       'ERROR_NO_RESTAURANT_FOUND' => 1001 ,
       'ERROR_INVALID_REVIEW_DATA' => 1002,
       'ERROR_INVALID_ADD_TO_FAVOURITE_DATA' => 1003,
       'ERROR_INVALID_RESTAURANT_ID' => 1004
    ],

    'review_type' => [
        'Restaurant',
        'Menu Item',
        'Order',
        'Delivery Agent'
    ]
];

?>