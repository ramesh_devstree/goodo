@extends('layout.resetpassword.main')

<style>
    .email-error, .email-success {
        display: none;
    }    
</style>

@section('content')

<div class="col-md-5">
    <div class="form-input-content">

        <!-- Display success/errors message -->
        <div class="alert alert-danger email-error"></div>
        <div class="alert alert-success email-success"></div>

        <div class="card card-login">
            <div class="card-body">
                <form id="forgotEmailForm">
                    <div class="form-group mb-4">
                        <input 
                            type="email" 
                            class="form-control rounded-0 bg-transparent" 
                            placeholder="{{ __('Enter email') }}" 
                            id="email"
                            name="email"
                            data-url="{{route('forgot-password-verify-mail')}}">
                    </div>
                    <button class="btn btn-primary btn-block border-0">{{ __('Send reset link') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection