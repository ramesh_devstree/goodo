@extends('layout.resetpassword.main')

@section('content')

<div class="col-md-5">

    @if ($errors->first('invalid_link') !== '')

        <div class="alert alert-danger">
            <span> {{ $errors->first('invalid_link') }} </span>
        </div>

    @else

        <div class="form-input-content">

            <!-- if there are reset password errors, show them here -->
            @if($errors->first('password') || 
                $errors->first('confirm_password')
            )
                <div class="alert alert-danger">
                    <span> {{ $errors->first('password') }} </span>
                    <span> {{ $errors->first('confirm_password') }} </span>
                </div>
            @endif

            @if($errors->first('not_match_password'))
                <div class="alert alert-danger">
                    <span> {{ $errors->first('not_match_password') }} </span> <br>
                </div>
            @endif
            <!-- End - if there are reset password errors, show them here -->
            
            <!-- card login div -->
            @if (isset($success))

                <div class="alert alert-success">
                {{ $success }}, {{ __('Back to') }} <a href={{url('login')}}> {{ __('Login') }} </a> {{ __('page') }} 
                </div>

                @else

                <div class="card card-login">
                    <div class="card-header">
                        <div class="position-relative  text-center w-100">
                            <div class="br
                    </div>
                    
                    <p> {{$token}} </p>
                    <div class="card-body">
                        <form action="{{url('password/forgot/'.$token.'?email='.$email)}}" method="post">
                            @csrf
                            <input type="hidden" value="{{$token}}" name="token">
                            <div class="form-group mb-4">
                                <input type="password" class="form-control rounded-0 bg-transparent" placeholder="{{ __('New password') }} " name="new_password">
                            </div>
                            <div class="form-group mb-4">
                                <input type="password" class="form-control rounded-0 bg-transparent" placeholder="{{ __('Confirm password') }}" name="confirm_password">
                            </div>
                            <button class="btn btn-primary btn-block border-0" type="submit"> {{ __('Submit') }}</button>
                        </form>
                    </div>
                </div>

            @endif
            
        </div>

    @endif

</div>
    
@endsection