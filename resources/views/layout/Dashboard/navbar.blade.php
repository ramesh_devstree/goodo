<div class="nav-header">
    <div class="brand-logo">
        <a href="index.html">
            <b class="logo-abbr">G</b>
            <span class="brand-title"><b>Goodo</b></span>
        </a>
    </div>
    <div class="nav-control">
        <div class="hamburger">
            <span class="toggle-icon"><i class="icon-menu"></i></span>
        </div>
    </div>
</div>