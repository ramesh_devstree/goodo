<div class="nk-sidebar">           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label"> {{ __('dashboard') }}</li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-speedometer"></i><span class="nav-text"> {{ __('Dashboard') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('reset-password-message-view') }}">
                    <i class="icon-speedometer"></i><span class="nav-text"> {{ __('reset_password') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}">
                    <i class="icon-speedometer"></i><span class="nav-text"> {{ __('Logout') }} </span>
                </a>
            </li>
            <li>
                <a href="{{ route('login') }}">
                    <i class="icon-speedometer"></i><span class="nav-text"> {{ __('add_region') }} </span>
                </a>
                <a href="{{ route('login') }}">
                    <i class="icon-speedometer"></i><span class="nav-text"> {{ __('add_restaurant') }} </span>
                </a>
            </li>
        </ul>
    </div>
</div>