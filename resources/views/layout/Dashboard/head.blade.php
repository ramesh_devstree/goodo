<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Goodo - Dashboard</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">
<!-- Custom Stylesheet -->
<link rel="stylesheet" href="{{asset('assets/plugins/owl.carousel/dist/css/owl.carousel.min.css')}}">
<link href="{{asset('assets/plugins/fullcalendar/css/fullcalendar.min.css')}}" rel="stylesheet">
<!-- Chartist -->
<link rel="stylesheet" href="{{asset('assets/plugins/chartist/css/chartist.min.css')}}">
<link href="{{asset('css/style.css')}}" rel="stylesheet">