
<!--**********************************
    Scripts
***********************************-->
<script src="{{ asset('assets/plugins/common/common.min.js')}}"></script>
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('js/settings.js') }}"></script>
<script src="{{ asset('js/quixnav.js') }}"></script>
<script src="{{ asset('js/styleSwitcher.js') }}"></script>

<!-- Datamap -->
<script src="{{ asset('assets/plugins/d3v3/index.js')}}"></script>
<script src="{{ asset('assets/plugins/topojson/topojson.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datamaps/datamaps.world.min.js') }}"></script>
<!-- Calender -->
<script src="{{ asset('assets/plugins/jqueryui/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/plugins/fullcalendar/js/fullcalendar.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/plugins/chart.js/Chart.bundle.min.js') }}"></script>
<!-- MorrisJS -->
<script src="{{ asset('assets/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
<!-- Owl carousel -->
<script src="{{ asset('assets/plugins/owl.carousel/dist/js/owl.carousel.min.js') }}"></script>
<!-- Chartist -->
<script src="{{ asset('assets/plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ asset('assets/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js') }}"></script>


<!-- Init files -->
<script src="{{ asset('main/js/plugins-init/fullcalendar-init.js') }}"></script>
<script src="{{ asset('js/dashboard/dashboard-1.js') }}"></script>