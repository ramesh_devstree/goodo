<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>
    @include('layout.dashboard.head')
</head>

<div id="main-wrapper">

    @include('layout.dashboard.navbar')

    @include('layout.dashboard.header')

    @include('layout.dashboard.sidebar')

    @include('layout.dashboard.body')

</div>

@include('layout.dashboard.foot')

</html>