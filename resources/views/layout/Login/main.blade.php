<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>
    @include('layout.login.head')
</head>

@include('layout.login.body')

@include('layout.login.foot')

</html>