<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    @include('layout.login.head')
</head>

@include('layout.resetpassword.body')

@include('layout.login.foot')

</html>