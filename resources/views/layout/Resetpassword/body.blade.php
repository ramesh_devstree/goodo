<body class="h-100">
    <div class="login-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                @yield('content')
            </div>
        </div>
    </div>
    @stack('scripts')
</body>