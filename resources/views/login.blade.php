@extends('layout.login.main')

@section('content')

<div class="col-md-5">
    <div class="form-input-content">

        <!-- if there are login errors, show them here -->
        @if($errors->first('email') || $errors->first('password'))
            <div class="alert alert-danger">
                <span> {{ $errors->first('email') }} </span>
                <span> {{ $errors->first('password') }} </span>
            </div>
        @endif
        @if($errors->first('invalid_details'))
            <div class="alert alert-danger">{{ $errors->first('invalid_details') }}</div>
        @endif
        <!-- End - if there are login errors, show them here -->
        
        <!-- card login div -->
        <div class="card card-login">
            <div class="card-header">
                <div class="position-relative  text-center w-100">
                    <div class="br
            </div>
            <div class="text-center my-3">
                <img class="rounded-circle" src="{{asset('assets/images/avatar/11.png')}}" width="80" height="80" alt="">
            </div>
            
            <div class="card-body">
                <form action="{{url('/login')}}" method="post">
                    @csrf
                    <div class="form-group mb-4">
                        <input type="email" class="form-control rounded-0 bg-transparent" placeholder="{{ __('Email') }}" name="email">
                    </div>
                    <div class="form-group mb-4">
                        <input type="password" class="form-control rounded-0 bg-transparent" placeholder="{{ __('Password') }}" name="password">
                    </div>
                    <button class="btn btn-primary btn-block border-0" type="submit">{{ __('Login') }}</button>
                </form>
            </div>

            <div class="card-footer text-center border-0 pt-0">
                <a href="{{url('forgot-password/mail')}}"> {{ __("Forgot_password") }} </a>
            </div>
        </div>
    </div>
</div>

@endsection