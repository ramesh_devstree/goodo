@extends('layout.dashboard.main')

@section('content')
    <div class="col-xl-3 text-left">
        <h2 class="page-heading">Hi,Welcome Back!</h2>
        <p class="mb-0">Your restaurent admin template</p>
    </div>
    <div class="col-xl-9 mt-3 mt-xl-0">
        <div class="card mb-0">
            <div class="card-body">
                <h4 class="card-title">Track Order</h4>
                <div class="row">
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" class="form-control" value="ORDER-84534598">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">Track</button>
                            </div>
                        </div>
                        <span class="d-block mt-1">Your order is now preparing</span>
                    </div>
                    <div class="col-md-7 p-0 mt-3 mt-md-0">
                        <div class="steps">
                            <ul class="list-unstyled multi-steps">
                                <li>Ordered</li>
                                <li>Pending</li>
                                <li class="is-active">Preparing</li>
                                <li>Delivery</li>
                                <li>Received</li>
                            </ul>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection