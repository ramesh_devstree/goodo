@extends('layout.dashboard.main')

@section('content')

    @if ($errors->first() !== '')
        <div class="alert alert-danger">
            <span> {{ $errors->first() }} </span>
        </div>
    @else
        <p> {{ __('Link has been sent to email') }} <strong>{{ $email }}</strong>, {{ __('please check into your inbox') }} </p>
    @endif

@endsection