<?php 

return [
    
    /**
    * API success messages
    */
    'success' => [
        'MESSAGE_REGIONS_LISTING' => 'Region listed successfully',
        'MESSAGE_NO_RECORD_FOUND' => 'No recoed found in system',
        'MESSAGE_NEAR_BY_RESTAURANTS_LISTING' => 'Near by restaurants listed successfully',
        'MESSAGE_RESTAURANT_FOUND' => 'Restaurant found successfully',
        'MESSAGE_REVIEW_ADDED_SUCCESSFULLY' => 'Review added successfully',
        'MESSAGE_REVIEW_LISTED_SUCCESSFULLY' => 'Review listed successfully',
        'NO_DATA_FOUND' => 'Reecord not availble in the stystem',
        'MESSAGE_RESTAURANT_ADDED_TO_FAVOURITE' => 'Restaurant added to favourite',
        'MESSAGE_RESTAURANT_DELETED_FROM_FAVOURITE' => 'Restaurant removed from favourite',
    ],

    /**
    * API Error Codes and Message
    */
    'custom_error_message' => [
        'ERROR_INTERNAL_SERVER_ERROR' => 'Internal server error',
        'ERROR_NO_RESTAURANT_FOUND' => 'Restaurant not found'
    ]
];
