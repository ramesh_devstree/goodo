<?php 

return [
    
    /**
    * API success messages
    */
    'success' => [
        'MESSAGE_REGIONS_LISTING' => 'Region listed successfully chienese',
        'MESSAGE_NO_RECORD_FOUND' => 'No recoed found in system chienese',
        'MESSAGE_NEAR_BY_RESTAURANTS_LISTING' => 'Near by restaurants listed successfully chienese'
    ],

    /**
    * API Error Codes and Message
    */
    'custom_error_message' => [
        'ERROR_INTERNAL_SERVER_ERROR' => 'Internal server error chienese',
        'ERROR_NO_RESTAURANT_FOUND' => 'Restaurant not found chienese'
    ]
];
