<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AddNewRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $donationSetting = [
            ['name' => 'Mount Fortress', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Senado Square', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Kun Iam Tong', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Grand Prix Museum', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Macau Tower', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];

        foreach ($donationSetting as $item) {
            DB::table('regions')->insert($item);
        }
    }
}
