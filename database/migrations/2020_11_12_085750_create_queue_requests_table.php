<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQueueRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queue_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('queue_id', 100);
            $table->integer('no_of_guests');
            $table->enum('table_type', ['Whole Table','Partial Table']);
            $table->enum('status', ['Active','Pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue_requests');
    }
}
