<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantFloorTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_floor_tables', function (Blueprint $table) {
            $table->id();
            $table->integer('restaurant_floor_id')->unsigned();
            $table->string('name',255);
            $table->enum('type', ['Permanent', 'Casual']);
            $table->integer('no_of_seats')->default(0)->unsigned();
            $table->integer('guest_range_whole')->default(0)->unsigned();
            $table->integer('guest_range_partial')->default(0)->unsigned();
            $table->enum('style', ['Rectangle', 'Round','Half-Round']);
            $table->string('x_dimension',255);
            $table->string('y_dimension',255);
            $table->string('x_grid_position',255);
            $table->string('y_grid_position',255);
            $table->string('orientation',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_floor_tables');
    }
}
