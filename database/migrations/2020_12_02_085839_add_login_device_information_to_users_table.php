<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoginDeviceInformationToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('device_type', 50)->after('remember_token');
            $table->string('device_token', 255)->after('device_type');
            $table->string('device_id', 255)->after('device_token');
            $table->enum('login_type', ['google', 'facebook', 'email'])->after('device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('device_type');
            $table->dropColumn('device_token');
            $table->dropColumn('device_id');
            $table->dropColumn('login_type');
        });
    }
}
