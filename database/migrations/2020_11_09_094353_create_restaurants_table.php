<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->longText('address');
            $table->string('area', 255);
            $table->integer('postcode')->unsigned();
            $table->string('phone_number', 100);
            $table->enum('status', ['Active', 'Inactive']);
            $table->date('activation_date');
            $table->date('expiry_date');
            $table->integer('max_table_number')->default(0)->unsigned();
            $table->integer('max_menu_item')->default(0)->unsigned();
            $table->float('average_rating', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
