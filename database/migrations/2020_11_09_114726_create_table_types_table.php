<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_types', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['Numbered', 'Unnumbered', 'Waiting List']);
            $table->longText('description');
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('table_types')->insert(
            [
                array(
                    'type' => 'Numbered',
                    'description' => 'Table with individual table ID  and seat ID'
                ),
                array(
                    'type' => 'Unnumbered',
                    'description' => 'Table without individual table ID  and seat ID, and it is listed as Casual Table with total number of seats available'
                ),
                array(
                    'type' => 'Waiting List',
                    'description' => 'Table not available yet'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_types');
    }
}
