<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('icon',255)->nullable();
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('memberships')->insert(
            [
                array(
                    'name' => 'Non Member'
                ),
                array(
                    'name' => 'Member'
                ),
                array(
                    'name' => 'Gold Membership'
                ),
                array(
                    'name' => 'Platinum Membership'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
