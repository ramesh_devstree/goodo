<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryAgentTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_agent_trip', function (Blueprint $table) {
            $table->id();
            $table->integer('delivery_agent_id');
            $table->integer('order_id');
            $table->enum('status', ['Incoming', 'Accepted','Delivered','Done']);
            $table->enum('order_type', ['Order', 'Pickup','Dropoff']);
            $table->dateTime('delivered_at', 0)->nullable();
            $table->dateTime('picked_at', 0)->nullable();
            $table->dateTime('dropped_at', 0)->nullable();
            $table->float('total_distance',14,2)->default(0);
            $table->float('total_charge',14,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_agent_trip');
    }
}
