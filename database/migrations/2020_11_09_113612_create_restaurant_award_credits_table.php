<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantAwardCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_award_credits', function (Blueprint $table) {
            $table->id();
            $table->integer('restaurant_id')->unsigned();
            $table->integer('awrad_issued')->default(0)->unsigned();
            $table->integer('awrad_used')->default(0)->unsigned();
            $table->integer('awrad_unused')->default(0)->unsigned();
            $table->enum('available_for_selected_restaurant', ['Yes', 'No']);
            $table->enum('allowed_award_by_this_restaurant', ['Yes', 'No']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_award_credits');
    }
}
