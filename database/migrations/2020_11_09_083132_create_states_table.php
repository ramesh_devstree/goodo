<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->integer('country_id');
            $table->string('name', 100);
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('states')->insert(
            [
                array(
                    'country_id' =>  1,
                    'name' => 'Qingzhou'
                ),
                array(
                    'country_id' =>  1,
                    'name' => 'Taishan'
                ),
                array(
                    'country_id' =>  1,
                    'name' => 'Racecourse'
                ),
                array(
                    'country_id' =>  1,
                    'name' => 'Youhan'
                ),
                array(
                    'country_id' =>  1,
                    'name' => 'Black sand ring'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
