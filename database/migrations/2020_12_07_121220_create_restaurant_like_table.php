<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_like', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id', false)->unsigned();
            $table->bigInteger('restaurant_id', false)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            // Relation defined between restaurant(user_id) with user(user_id)
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');

            // Relation defined between restaurant(restaurant_id) with restaurants(id)
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_like');
    }
}
