<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('roles')->insert(
            [
                array(
                    'name' => 'System Admin'
                ),
                array(
                    'name' => 'System User'
                ),
                array(
                    'name' => 'System Support'
                ),
                array(
                    'name' => 'Accounting User'
                ),
                array(
                    'name' => 'Customer'
                ),
                array(
                    'name' => 'Operation Manager'
                ),
                array(
                    'name' => 'Restaurant Admin'
                ),
                array(
                    'name' => 'Restaurant Main User'
                ),
                array(
                    'name' => 'Restaurant Order Taker'
                ),
                array(
                    'name' => 'Restaurant Kitchen User'
                ),
                array(
                    'name' => 'Restaurant Runner User'
                ),
                array(
                    'name' => 'Restaurant Supervisor'
                ),
                array(
                    'name' => 'Restaurant Manager'
                ),
                array(
                    'name' => 'Restaurant Accounting User'
                ),
                array(
                    'name' => 'Restaurant Admin Support'
                ),
                array(
                    'name' => 'Delivery Agent'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
