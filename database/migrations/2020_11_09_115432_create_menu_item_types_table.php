<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item_types', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['Customer Item', 'Sharing Item', 'Shared Customer Item']);
            $table->longText('description');
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('menu_item_types')->insert(
            [
                array(
                    'type' => 'Customer Item',
                    'description' => 'Item is made available for customer menu selection and item source is from own restaurant.'
                ),
                array(
                    'type' => 'Sharing Item',
                    'description' => 'Item from own restaurant and is made available for other restaurants to use as their item'
                ),
                array(
                    'type' => 'Shared Customer Item',
                    'description' => 'Item is made available for customer menu selection, item source is another restaurant (eg using sharing item)'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item_types');
    }
}
