<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_menu_items', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->default(0)->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('item_type_id')->unsigned();
            $table->integer('kitchen_id')->unsigned();
            $table->string('name',255);
            $table->string('image',255);
            $table->string('menu_source_id',255);
            $table->float('menu_source_price',14,2)->default(0);
            $table->float('menu_item_price',14,2)->default(0);
            $table->longText('description')->nullable();
            $table->longText('comment')->nullable();
            $table->enum('is_market_price', ['Yes', 'No']);
            $table->enum('is_menu_setup', ['Yes', 'No']);
            $table->integer('no_of_items_included')->default(1)->unsigned();
            $table->integer('available_quantity')->unsigned();
            $table->enum('quantity_type', ['Daily', 'Absolute']);
            $table->date('available_from_date')->nullable();
            $table->date('available_to_date')->nullable();
            $table->enum('available_for_order_type', ['Home Delivery', 'Self Pickup', 'Dine In']);
            $table->float('average_rating',14,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_menu_items');
    }
}
