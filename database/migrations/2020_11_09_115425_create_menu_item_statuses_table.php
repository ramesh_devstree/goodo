<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item_statuses', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['Available', 'Sold Out', 'Unavailable','Inactive']);
            $table->longText('description');
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('menu_item_statuses')->insert(
            [
                array(
                    'status' => 'Available',
                    'description' => 'Currently availale for customer menu selection'
                ),
                array(
                    'status' => 'Sold Out',
                    'description' => 'Listed as Sold Out (as number of items sold reached total quantity)'
                ),
                array(
                    'status' => 'Unavailable',
                    'description' => 'Not listed for customer menu selection (due to time limit eg breakfast menu 6am-10am)'
                ),
                array(
                    'status' => 'Inactive',
                    'description' => 'Menu Item created by not listed for customer menu selection'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item_statuses');
    }
}
