<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('role_id');
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->longText('password');
            $table->string('phone_number', 100)->nullable();
            $table->string('whatsapp_wechat_id', 255)->nullable();
            $table->enum('status', ['Active', 'Inactive','Pending for Approval','Approved','Suspended','Withdrawn','Blocked','Locked']);
            $table->integer('approved_by')->nullable()->unsigned();
            $table->dateTime('approved_date', 0)->nullable();
            $table->longText('comment')->nullable();
            $table->string('latitude', 100)->nullable();
            $table->string('longitude', 100)->nullable();
            $table->float('credit_balance',14,2)->default(0);
            $table->float('reward_balance',14,2)->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
