<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_statuses', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['Unoccupied', 'Occupied', 'Reserved','Inactive']);
            $table->longText('description');
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('table_statuses')->insert(
            [
                array(
                    'status' => 'Unoccupied',
                    'description' => 'Table is available for customer'
                ),
                array(
                    'status' => 'Occupied',
                    'description' => 'Table is occupied by customer'
                ),
                array(
                    'status' => 'Reserved',
                    'description' => 'Table is reserved by customer'
                ),
                array(
                    'status' => 'Inactive',
                    'description' => 'Table is not available'
                )
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_statuses');
    }
}
