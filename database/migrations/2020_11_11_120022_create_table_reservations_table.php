<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_reservations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('restaurant_id');
            $table->date('date');
            $table->integer('no_of_guests');
            $table->time('time', 0);
            $table->enum('table_type', ['Whole Table','Partial Table']);
            $table->enum('require', ['Driver Pick-up','Driver Drop-off']);
            $table->longText('pickup_address')->nullable();
            $table->integer('pickup_no_of_guests')->default(0);
            $table->longText('dropoff_address')->nullable();
            $table->integer('dropoff_no_of_guests')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_reservations');
    }
}
