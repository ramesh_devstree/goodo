<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_devices', function (Blueprint $table) {
            $table->id();
            $table->string('device_id', 100);
            $table->string('name', 255);
            $table->enum('type', ['Reception', 'Order Taker','Kitchen','Runner','Agent','Cashier']);
            $table->enum('status', ['Active', 'Inactive','Logged In','Disabled']);
            $table->integer('last_login_user')->unsigned();
            $table->dateTime('last_login_time', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_devices');
    }
}
