<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('address_id')->nullable();
            $table->integer('restaurant_id');
            $table->string('order_number',255);
            $table->dateTime('date',0);
            $table->enum('type', ['Delivery', 'Pick-Up', 'Dine-In'])->default('Delivery');
            $table->float('delivery_cost',14,2)->default(0);
            $table->longText('instruction')->nullable();
            $table->dateTime('cooking_time',0);
            $table->integer('no_of_guests')->nullable();
            $table->string('suburb',255)->nullable();
            $table->longText('pickup_address')->nullable();
            $table->float('pickup_estimated_cost',14,2)->default(0);
            $table->longText('dropoff_address')->nullable();
            $table->float('dropoff_estimated_cost',14,2)->default(0);
            $table->integer('coupencode_id')->nullable();
            $table->float('discount_amount',14,2)->default(0);
            $table->float('tip_amount',14,2)->default(0);
            $table->float('service_charge',14,2)->default(0);
            $table->float('tax_charge',14,2)->default(0);
            $table->float('reward_earned_amount',14,2)->default(0);
            $table->float('sub_total',14,2)->default(0);
            $table->float('grand_total',14,2)->default(0);
            $table->enum('payment_method', ['MPay', 'Apple Pay', 'Ali Pay','Wechat Pay','Credit Card','Cash','Google Pay']);
            $table->enum('payment_status', ['Paid', 'Unpaid']);
            $table->enum('order_status', ['Submitted', 'Cancelled', 'Delivered']);
            $table->longText('cancel_notes')->nullable();
            $table->integer('restaurant_rating')->nullable();
            $table->longText('restaurant_comment')->nullable();
            $table->integer('service_agent_rating')->nullable();
            $table->longText('service_agent_comment')->nullable();
            $table->integer('received_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
