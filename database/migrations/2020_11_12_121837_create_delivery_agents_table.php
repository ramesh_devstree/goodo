<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_agents', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->date('birthdate')->nullable();
            $table->enum('gender', ['Male', 'Female'])->nullable();
            $table->string('account_id', 100)->nullable();
            $table->string('emergency_phone_number', 100)->nullable();
            $table->string('type_of_car',255);
            $table->string('driving_licence_number',255);
            $table->string('vehicle_number',255);
            $table->string('driving_licence',255);
            $table->string('other_transportation_tool',255);
            $table->string('home_suburb',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_agents');
    }
}
